package net.metromc.game.user.listener;

import net.metromc.game.GamePlugin;
import net.metromc.game.user.MetroUser;
import net.metromc.spigot.user.event.UserLoadEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class MetroUserListener implements Listener {

    private final GamePlugin gamePlugin;

    public MetroUserListener(GamePlugin gamePlugin) {
        this.gamePlugin = gamePlugin;
    }

    @EventHandler
    public void onJoin(UserLoadEvent e) {
        MetroUser metroUser = new MetroUser(gamePlugin, e.getUser());

        e.getUser().getPlayer().getInventory().clear();
        metroUser.loadInventory();

        gamePlugin.getMetroUserManager().addUser(metroUser);
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        MetroUser metroUser = gamePlugin.getMetroUserManager().getUser(e.getPlayer().getName());

        metroUser.saveInventory();
        metroUser.getUser().getPlayer().getInventory().clear();

        gamePlugin.getMetroUserManager().removeUser(metroUser.getUser().getName());
    }
}
