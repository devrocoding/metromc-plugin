package net.metromc.game.user;

import com.google.common.collect.Maps;
import java.util.Map;
import net.metromc.game.GamePlugin;
import net.metromc.game.user.listener.MetroUserListener;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.Module;

public class MetroUserManager extends Module {

    private final Map<String, MetroUser> metroUsers = Maps.newHashMap();

    public MetroUserManager(MetroMC plugin, GamePlugin gamePlugin) {
        super(plugin, "Metro User Manager");

        registerListener(new MetroUserListener(gamePlugin));
    }

    public MetroUser getUser(String name) {
        return metroUsers.get(name.toUpperCase());
    }

    public void addUser(MetroUser user) {
        metroUsers.put(user.getUser().getName().toUpperCase(), user);
    }

    public void removeUser(String name) {
        metroUsers.remove(name.toUpperCase());
    }
}
