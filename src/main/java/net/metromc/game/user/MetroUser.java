package net.metromc.game.user;

import java.util.Map;
import lombok.Getter;
import net.metromc.game.GamePlugin;
import net.metromc.spigot.user.object.User;
import net.metromc.spigot.util.UtilInventory;
import org.bson.Document;

public class MetroUser {

    private final GamePlugin gamePlugin;
    @Getter
    private final User user;
    @Getter
    private Document document;

    public MetroUser(GamePlugin gamePlugin, User user) {
        this.gamePlugin = gamePlugin;
        this.user = user;

        if (user.get("metro") != null) {
            this.document = (Document) user.get("metro");
        } else {
            this.document = new Document();
        }
    }

    public void loadInventory() {
        UtilInventory.fillInventory(user.getPlayer().getInventory(), (Map) document.get("inventory"));
    }

    public void saveInventory() {
        document.put("inventory", UtilInventory.getInventory(user.getPlayer().getInventory()));
    }

    public long getBullets() {
        return document.getLong("bullets");
    }

    public void setBullets(Long bullets) {
        document.put("bullets", bullets);
    }

    public void update() {
        user.put("metro", document);
    }
}
