package net.metromc.game;

import lombok.Getter;
import net.metromc.game.weapon.WeaponManager;
import net.metromc.game.user.MetroUserManager;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.api.MetroMcApi;
import net.metromc.spigot.hologram.Hologram;
import net.metromc.spigot.npc.NPC;
import net.metromc.spigot.npc.npcs.EntityNPC;
import net.metromc.spigot.npc.npcs.HumanNPC;
import net.metromc.spigot.world.object.Region;
import net.minecraft.server.v1_12_R1.EntityCreeper;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class GamePlugin extends JavaPlugin {

    @Getter
    private MetroUserManager metroUserManager;
    @Getter
    private WeaponManager weaponManager;

    @Override
    public void onEnable() {
        MetroMC metroMC = MetroMcApi.getAPI();

        this.metroUserManager = new MetroUserManager(metroMC, this);
        this.weaponManager = new WeaponManager(metroMC);

        Location location = metroMC.getWorldManager().getWorldData().getSpawn("location").getLocation();

        metroMC.getRunnableManager().createRunnable("Runnable", plugin -> {
            Bukkit.broadcastMessage("Runnables, yay");
        }).runTaskLater(metroMC, 20);


    }

    @Override
    public void onDisable() {

    }
}
