package net.metromc.game.weapon;

import com.google.common.collect.Maps;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;
import net.metromc.game.weapon.object.Weapon;
import net.metromc.game.weapon.object.WeaponType;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.Module;
import net.metromc.spigot.util.D;
import org.bukkit.inventory.ItemStack;

public class WeaponManager extends Module {

    private final static Map<String, Weapon> WEAPONS = Maps.newHashMap();

    public WeaponManager(MetroMC plugin) {
        super(plugin, "Weapon Manager");

        loadWeaponsFromJson();
    }

    public void loadWeaponsFromJson() {
        WEAPONS.clear();

        for (File file : new File(getPlugin().getDataFolder() + File.separator + "weapons").listFiles()) {
            try {
                JsonObject jsonObject = new JsonParser().parse(new JsonReader(new FileReader(file.getPath()))).getAsJsonObject();
                WeaponType type = WeaponType.valueOf(jsonObject.get("type").getAsString());

                JsonArray jsonArray = jsonObject.get("weapons").getAsJsonArray();

                jsonArray.forEach(jsonElement -> {
                    Weapon weapon = Weapon.fromJson(jsonElement.getAsJsonObject(), type);
                    WEAPONS.put(weapon.getName().toUpperCase(), weapon);
                });
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            }
        }

        D.d("successfully loaded all weapons!");
    }

    public Weapon getWeapon(int id) {
        for (String s : WEAPONS.keySet()) {
            Weapon weapon = WEAPONS.get(s);

            if (weapon.getId() == id) {
                return weapon;
            }
        }

        return null;
    }

    public Weapon getWeapon(ItemStack itemStack) {
        return getWeapon(itemStack.getDurability());
    }

}