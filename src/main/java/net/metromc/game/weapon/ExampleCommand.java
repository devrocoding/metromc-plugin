package net.metromc.game.weapon;

import net.metromc.spigot.MetroMC;
import net.metromc.spigot.command.Command;
import net.metromc.spigot.user.object.Rank;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ExampleCommand extends Command {

    public ExampleCommand(MetroMC plugin) {
        super(plugin, Rank.ADMIN, "Example description", "example", "second alias");

        setCooldown(20); // one second cooldown
        setUsage("<player>", "<action>");
    }


    @Override
    public void execute(Player player, String[] args) {
        if (!checkArgs(args)) { //Check if the size of arguments is equal to the amount of strings supplied in the constructor
            player.sendMessage("Invalid arguments");

            return;
        }


        Bukkit.broadcastMessage("Correct amount of arguments");
        player.getInventory().addItem(new ItemStack(Material.YELLOW_FLOWER));
    }
}
