package net.metromc.game.weapon.object;

import com.google.gson.JsonObject;
import lombok.Getter;
import net.metromc.spigot.util.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Weapon {

    @Getter
    private final String name;
    @Getter
    private final int id;
    @Getter
    private final WeaponType weaponType;
    @Getter
    private final ItemStack itemStack;
    @Getter
    private final int magazine;
    @Getter
    private final double bulletDamage;
    @Getter
    private final float recoil;
    @Getter
    private final float bulletDrop;

    public Weapon(String name, int id, WeaponType weaponType, ItemStack itemStack, int magazine, double bulletDamage, float recoil, float bulletDrop) {
        this.name = name;
        this.id = id;
        this.weaponType = weaponType;
        this.itemStack = itemStack;
        this.magazine = magazine;
        this.bulletDamage = bulletDamage;
        this.recoil = recoil;
        this.bulletDrop = bulletDrop;
    }

    public static Weapon fromJson(JsonObject jsonObject, WeaponType weaponType) {
        String name = jsonObject.get("name").getAsString();
        int id = jsonObject.get("item_damage").getAsInt();
        ItemStack item = new ItemBuilder(Material.getMaterial(jsonObject.get("material").getAsString()), 1, id).build();
        int magazine = jsonObject.get("magazine").getAsInt();
        double bulletDamage = jsonObject.get("damage").getAsDouble();
        float recoil = jsonObject.get("recoil").getAsFloat();
        float bulletDrop = jsonObject.get("bulletdrop").getAsFloat();

        return new Weapon(name, id, weaponType, item, magazine, bulletDamage, recoil, bulletDrop);
    }
}
