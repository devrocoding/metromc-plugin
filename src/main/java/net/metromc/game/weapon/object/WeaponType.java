package net.metromc.game.weapon.object;

public enum WeaponType {

    MELEE,
    HAND_GUN,
    ASSAULT_RIFLE,
    SHOTGUN,
    PNEUMATIC,
    HEAVY,
    EXPLOSIVE,
    EMPLACEMENT;

}
