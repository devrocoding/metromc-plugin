package net.metromc.game.weapon.listener;

import net.metromc.game.GamePlugin;
import net.metromc.game.weapon.object.Weapon;
import net.metromc.spigot.util.D;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

public class WeaponListener implements Listener {

    private final static Integer MAX_RANGE = 100;
    private final static Material WEAPON_MATERIAL = Material.DIAMOND_HOE;

    private final GamePlugin gamePlugin;

    public WeaponListener(GamePlugin gamePlugin) {
        this.gamePlugin = gamePlugin;
    }

    @EventHandler
    public void attemptShoot(PlayerInteractEvent e) {
        if (!e.getPlayer().getInventory().getItemInMainHand().getType().equals(WEAPON_MATERIAL)) {
            return;
        }

        if (e.getAction().equals(Action.LEFT_CLICK_BLOCK) || e.getAction().equals(Action.LEFT_CLICK_AIR)) {
            ItemStack itemStack = e.getPlayer().getInventory().getItemInMainHand();
            Weapon weapon = gamePlugin.getWeaponManager().getWeapon(itemStack);

            if (weapon == null) {
                D.d("Weapon not found returning...");
                return;
            }

            if (!itemStack.hasItemMeta()) {
                return;
            }

            if (!itemStack.getItemMeta().hasDisplayName()) {
                return;
            }

            Integer ammo = Integer.valueOf(itemStack.getItemMeta().getDisplayName().replace(" ", "").split("-")[1]);

            if (ammo <= 0) {
                return;
            }

            ItemMeta itemMeta = itemStack.getItemMeta();
            itemMeta.setDisplayName(weapon.getName() + " - " + (ammo - 1));
            itemStack.setItemMeta(itemMeta);

            fireGun(e.getPlayer(), weapon);
        }
    }

    private void fireGun(Player player, Weapon weapon) {
        shoot(player, weapon.getRecoil(), weapon.getBulletDrop(), weapon.getBulletDamage());
    }

    private void shoot(Player p, float recoil, float drop, double damage) {
        Vector direction = p.getLocation().getDirection().multiply(0.5);
        Location loc = p.getLocation();

        p.getWorld().playSound(p.getLocation(), Sound.ENTITY_FIREWORK_BLAST, 1F, 0F);
        p.getWorld().playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_INFECT, 1F, 0F);
        p.setVelocity(p.getLocation().getDirection().multiply(-recoil).setY(0.0D));

        loc.setY(p.getLocation().getY() + 1.5);

        p.playSound(p.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 4, 0);

        for (int i = 0; i < MAX_RANGE; i++) {
            Material material = loc.getBlock().getType();

            loc.add(direction);
            loc.setY(loc.getY() - drop);

            Bukkit.getOnlinePlayers().forEach(pl -> {
                if (loc.distance(pl.getLocation()) < 1) {
                    pl.damage(damage);

                    loc.getWorld().playSound(loc, Sound.ENTITY_ARROW_HIT, 4, 3);
                    loc.getWorld().playEffect(loc, Effect.STEP_SOUND, Material.REDSTONE_BLOCK);

                    return;
                }
            });

            if (material != Material.AIR || i == MAX_RANGE - 1) {
                loc.getWorld().playEffect(loc, Effect.STEP_SOUND, material);
                loc.getWorld().playSound(loc, Sound.BLOCK_DISPENSER_LAUNCH, 4, 10);
            }
            //TODO display particles
        }
    }


}
