package net.metromc.game.map.object;

import net.metromc.game.team.Team;
import net.metromc.spigot.world.object.Region;
import org.bukkit.Location;

public class MapPart {

    private final String name;

    private final MapPartType type;

    private final Team defaultOwner;
    private Team owner;

    private final Region region;
    private final Location spawnPoint;
    private final Location center;

    public MapPart(String name, MapPartType type, Team defaultOwner, Team owner, Region region, Location spawnPoint, Location center) {
        this.name = name;
        this.type = type;
        this.defaultOwner = defaultOwner;
        this.owner = owner;
        this.region = region;
        this.spawnPoint = spawnPoint;
        this.center = center;
    }

    public String getName() {
        return name;
    }

    public MapPartType getType() {
        return type;
    }

    public Team getDefaultOwner() {
        return defaultOwner;
    }

    public Team getOwner() {
        return owner;
    }

    public void setOwner(Team owner) {
        this.owner = owner;
    }

    public Region getRegion() {
        return region;
    }

    public Location getSpawnPoint() {
        return spawnPoint;
    }

    public Location getCenter() {
        return center;
    }
}
