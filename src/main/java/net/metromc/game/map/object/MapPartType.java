package net.metromc.game.map.object;

import lombok.Getter;

public enum MapPartType {
    SKY(true, false),
    UNDERGROUND(false, true),
    GROUND(true, true),
    SPAWN(false, false);

    @Getter
    private final boolean PVP;
    @Getter
    private final boolean PVE;

    MapPartType(boolean PVP, boolean PVE) {
        this.PVP = PVP;
        this.PVE = PVE;
    }
}
